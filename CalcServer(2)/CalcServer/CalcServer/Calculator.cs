﻿using System;
using System.Security.Authentication;
using System.Security.Principal;
using System.ServiceModel;
using System.ServiceModel.Configuration;

namespace CalcServer
{
    public class Calculator : ICalculator
    {
        public Result Add(Arguments args)
        {
          
            var requestIdentity = ServiceSecurityContext.Current.WindowsIdentity;
            Console.WriteLine($"Called by {requestIdentity.Name}");

            var threadIdentity = WindowsIdentity.GetCurrent();
            Console.WriteLine($"Thread identity: {threadIdentity.Name}");
            //WindowsImpersonationContext impersonationContext = null;
            //try
            //{
            //    impersonationContext = requestIdentity.Impersonate();
            //    var threadIdentityAfterImpersonation = WindowsIdentity.GetCurrent();
            //    //Operation/Call auf ressource
            //    Console.WriteLine($"Thread identity after impersonation: {threadIdentityAfterImpersonation.Name}");
            //}
            //finally
            //{
            //    if (impersonationContext != null)
            //    {
            //        impersonationContext.Undo();
            //        impersonationContext.Dispose();
            //    }
            //}

            using (var impersonationContext = requestIdentity.Impersonate())
            {
                var threadIdentityAfterImpersonation = WindowsIdentity.GetCurrent();
                //Operation/Call auf ressource
                Console.WriteLine($"Thread identity after impersonation: {threadIdentityAfterImpersonation.Name}");
                impersonationContext.Undo();
            }

            var threadIdentityAfterUndo = WindowsIdentity.GetCurrent();
            Console.WriteLine($"Thread identity after undo: {threadIdentityAfterUndo.Name}");
            //if (identity.Name.Contains( "Hugo"))  //<= Nie auf contains prüfen. Mit Domäne auf == prüfen
            //{
            //    //ok
            //}
            //else
            //{
            //    throw new InvalidCredentialException("Depp");
            //}
            //Console.WriteLine(identity.Name);


            if (args == null)
            {
                throw new ArgumentNullException();
            }
            return new Result
            {
                Value = args.Arg1 + args.Arg2
            };

            //var result = new Result
            //{
            //    Value = args.Arg1 + args.Arg2
            //};


            //result.Value = args.Arg1 + args.Arg2;
            //return result;
            /* Mehrzeilige Kommentare
            //throw new NotImplementedException(); Einzeilige Kommentare
            */
        }
    }
}