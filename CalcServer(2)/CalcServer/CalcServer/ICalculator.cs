﻿using System.Collections.Generic;
using System.Linq;
using System.Runtime.Hosting;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace CalcServer
{
    [ServiceContract]
    public interface ICalculator
    {
        [OperationContract]
        Result Add(Arguments arg);
    }
}
