﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace CalcServer
{
    class Program
    {
        static void Main(string[] args)
        {
            var host = new ServiceHost(typeof(Calculator));
            host.Open();

            Console.WriteLine("Service started. Press any key to exit");
            Console.ReadLine();
            host.Close();
        }
    }
}
