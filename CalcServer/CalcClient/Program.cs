﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CalcClient.ServiceReference1;

namespace CalcClient
{
    class Program
    {
        static void Main(string[] args)
        {
            CalculatorClient client = new CalculatorClient("NetTcpBinding_ICalculator");
            Arguments arguments = new Arguments();
            arguments.Arg1 = 2;
            arguments.Arg2 = 3.14;
            Result result = client.Add(arguments);
            Console.WriteLine($"{arguments.Arg1} + {arguments.Arg2} = {result.Value}");
            Console.WriteLine("Press any key to close");
            Console.ReadLine();
        }
    }
}
