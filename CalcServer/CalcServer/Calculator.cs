﻿using System;
using System.ServiceModel.Configuration;

namespace CalcServer
{
    public class Calculator : ICalculator
    {
        public Result Add(Arguments args)
        {
            if (args == null) { throw new ArgumentNullException(); }
            return new Result
            {
                Value = args.Arg1 + args.Arg2
            };

            //var result = new Result
            //{
            //    Value = args.Arg1 + args.Arg2
            //};


            //result.Value = args.Arg1 + args.Arg2;
            //return result;
            /* Mehrzeilige Kommentare
            //throw new NotImplementedException(); Einzeilige Kommentare
            */
        }
    }
}