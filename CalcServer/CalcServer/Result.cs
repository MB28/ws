﻿using System.Runtime.Serialization;

namespace CalcServer
{
    [DataContract]
    public class Result
    {
        [DataMember]
        public double Value { get; set; }
    }
}