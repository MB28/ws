﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using MessagingSharedContract;
using Microsoft.Azure.ServiceBus;

namespace MessagingCalculatorClient
{
    class Program
    {
        private static QueueClient m_ResultQueueClient;
        private static Dictionary<string, Arguments> m_Storage = new Dictionary<string, Arguments>();

        static async Task Main(string[] args)
        {
            var messageHandlerOptions = new MessageHandlerOptions(ExceptionReceivedHandler)
            {
                MaxConcurrentCalls = 1,
                AutoComplete = false
            };
            m_ResultQueueClient = new QueueClient(Constants.ServiceBusConnectionString, Constants.ResultQueueName);
            m_ResultQueueClient.RegisterMessageHandler(MessageHandler, messageHandlerOptions);

            for (int i = 0; i < 1000; i++)
            {
                var arguments = new Arguments();
                arguments.Arg1 = 1;
                arguments.Arg2 = 3.14;

                var messageBody = Newtonsoft.Json.JsonConvert.SerializeObject(arguments);
                var queueClient = new QueueClient(Constants.ServiceBusConnectionString, Constants.QueueName);
                var correlationId = Guid.NewGuid().ToString();
                await queueClient.SendAsync(new Message
                {
                    Body = Encoding.UTF8.GetBytes(messageBody),
                    CorrelationId = correlationId
                });
                lock (m_Storage)
                {
                    m_Storage[correlationId] = arguments;
                }

                Console.WriteLine($"Message send: Body: {messageBody} Correlation: {correlationId}");
            }


            
            Console.WriteLine("Press any key to exit");
            Console.ReadLine();
        }
        private static Task ExceptionReceivedHandler(ExceptionReceivedEventArgs arg)
        {
            Console.WriteLine($"Ups: {arg.Exception}");
            return Task.CompletedTask;
        }

        private static async Task MessageHandler(Message message, CancellationToken cancellationToken)
        {
            var messageBody = Encoding.UTF8.GetString(message.Body);
            var result = Newtonsoft.Json.JsonConvert.DeserializeObject<Result>(messageBody);
            Console.WriteLine($"Message (result) read: Body: {messageBody} Correlation: {message.CorrelationId}");

            lock (m_Storage)
            {
                if (m_Storage.TryGetValue(message.CorrelationId, out Arguments arguments))
                {
                    m_Storage.Remove(message.CorrelationId);
                    Console.WriteLine($"{arguments.Arg1} + {arguments.Arg2} = {result.Value}");
                }
            }

            await m_ResultQueueClient.CompleteAsync(message.SystemProperties.LockToken);
        }
    }
}
