﻿using System;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using MessagingSharedContract;
using Microsoft.Azure.ServiceBus;

namespace MessagingCalculatorService
{
    class Program
    {
        private static QueueClient m_QueueClient;

        static async Task Main(string[] args)
        {
            //var myStrangeJson = "{ \"Arg1\":1.0,\"Arg2\":\"blub\"}";
            //var arguments = Newtonsoft.Json.JsonConvert.DeserializeObject<Arguments>(myStrangeJson);

            m_QueueClient = new QueueClient(Constants.ServiceBusConnectionString, Constants.QueueName);
            var messageHandlerOptions = new MessageHandlerOptions(ExceptionReceivedHandler)
            {
                MaxConcurrentCalls = 1,
                AutoComplete = false
            };

            m_QueueClient.RegisterMessageHandler(MessageHandler, messageHandlerOptions);
            Console.WriteLine("Press any key to exit");
            Console.ReadLine();
        }

        private static Task ExceptionReceivedHandler(ExceptionReceivedEventArgs arg)
        {
            Console.WriteLine($"Ups: {arg.Exception}");
            return Task.CompletedTask;
        }

        private static async Task MessageHandler(Message message, CancellationToken cancellationToken)
        {
            var messageBody = Encoding.UTF8.GetString(message.Body);
            var arguments = Newtonsoft.Json.JsonConvert.DeserializeObject<Arguments>(messageBody);
            Console.WriteLine($"Message read: Body: {messageBody} Correlation: {message.CorrelationId}");
            
            //Jetzt operation ausführen
            var result = new Result { Value = arguments.Arg1 + arguments.Arg2 };
            var resultMessageBody = Newtonsoft.Json.JsonConvert.SerializeObject(result);
            var resultQueueClient = new QueueClient(Constants.ServiceBusConnectionString, Constants.ResultQueueName);
            var correlationId = message.CorrelationId;
            await resultQueueClient.SendAsync(new Message
            {
                Body = Encoding.UTF8.GetBytes(resultMessageBody),
                CorrelationId = correlationId
            });


            await m_QueueClient.CompleteAsync(message.SystemProperties.LockToken);
        }
    }
}
