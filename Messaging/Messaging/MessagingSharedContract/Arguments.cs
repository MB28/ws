﻿using System;

namespace MessagingSharedContract
{
    public class Arguments
    {
        public double Arg1 { get; set; }
        public double Arg2 { get; set; }
    }
}
