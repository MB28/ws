﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MyFirstServiceConsuer.ServiceReference1;

namespace MyFirstServiceConsuer
{
    class Program
    {
        static void Main(string[] args)
        {
            ServiceReference1.BLZServicePortTypeClient client=new BLZServicePortTypeClient("BLZServiceSOAP11port_http");
            string blz = "43060967";
            ServiceReference1.detailsType bank = client.getBank(blz);

            
            Console.WriteLine("Die {0} gehört zu {1} mit der BIC {2} in {3}", blz,bank.bezeichnung,bank.bic,bank.ort);

            Console.ReadLine();
        }
    }
}
