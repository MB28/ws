﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SimpleCustomerDemo.Models;

namespace SimpleCustomerDemo.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FavoriteCustomersController : ControllerBase
    {
        [HttpGet]
        public ActionResult<IEnumerable<Customer>> Get()
        {
            var allCustomers = CustomerRepository.GetAll();
            var favoriteCustomers = new List<Customer>();
            foreach (var customer in allCustomers)
            {
                if (customer.IsFavorite)
                {
                    favoriteCustomers.Add(customer);
                }
            }
            return favoriteCustomers;
        }
        [HttpPost]
        public void Post([FromBody]string id)
        {
            var internalId = 3; //ToDo: Get it from id (URI)
            if (CustomerRepository.TryGet(internalId, out Customer customer))
            {
                customer.IsFavorite = true;
                CustomerRepository.Update(customer);
            }
        }
    }
}