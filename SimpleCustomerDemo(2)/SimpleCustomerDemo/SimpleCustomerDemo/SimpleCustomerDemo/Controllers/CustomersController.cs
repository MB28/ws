﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CustomerDemo.Hypermedia;
using Microsoft.AspNetCore.Mvc;
using SimpleCustomerDemo.Models;

namespace SimpleCustomerDemo.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CustomersController : ControllerBase
    {
        // GET api/values
        [HttpGet(Name = "GETALLCUSTOMERS")]
        public Siren Get()
        {
            var siren = new Siren();
            siren.Class.Add("customers");
            siren.Class.Add("collection");
            foreach (var customer in CustomerRepository.GetAll())
            {
                var entity = new EmbeddedRepresentation();
                entity.Class.Add("customer");
                entity.Properties.Add(new Property { Name = "Name", Value = customer.Name });
                entity.Properties.Add(new Property { Name = "Country", Value = customer.Country });
                entity.Relation.Add("CustomerOverview");
                entity.Links.Add(new Link
                {
                    Relation = new List<string>() { "self" },
                    Href = Url.Link("GETCUSTOMER", new { id = customer.Id })
                });
                siren.Entities.Add(entity);
            }
            siren.Links.Add(new Link
            {
                Relation = new List<string>() { "self" },
                Href = Url.Link("GETALLCUSTOMERS", null)
            });

            return siren;
        }

        // GET api/values/5
        [HttpGet("{id}", Name = "GETCUSTOMER")]
        public IActionResult Get(int id)
        {
            if (CustomerRepository.TryGet(id, out Customer customer))
            {
                var siren = new Siren();
                var customerUri = Url.Link("GETCUSTOMER", new {Id = id});
                siren.Class.Add("customer");
                siren.Properties.Add(new Property { Name = "Name", Value = customer.Name });
                siren.Properties.Add(new Property { Name = "Street", Value = customer.Street });
                siren.Properties.Add(new Property { Name = "ZipCode", Value = customer.ZipCode });
                siren.Properties.Add(new Property { Name = "City", Value = customer.City});
                siren.Properties.Add(new Property { Name = "Country", Value = customer.Country });
                if (!customer.IsFavorite)
                {
                    var action = new CustomerDemo.Hypermedia.Action
                    {
                        Name = "MarkAsFavorite",
                        Method = "POST",
                        Type = "string",
                        Href = Url.Link("MARKCUSTOMERASFAVORITE", null)
                    };
                    action.Fields.Add(customerUri);  //Siehe Diskussion in der Vorlesung
                    siren.Actions.Add(action);
                }

                siren.Links.Add(new Link
                {
                    Relation = new List<string>() { "self" },
                    Href = customerUri
                });
                return new OkObjectResult(siren);
            }
            return new NotFoundResult();
        }

        // POST api/values
        [HttpPost]
        public IActionResult Post([FromBody] Customer value)
        {
            int newInternalId = CustomerRepository.Add(value);
            return new CreatedAtRouteResult("GETCUSTOMER", new { id = newInternalId }, null);
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] Customer value)
        {
            CustomerRepository.Update(value);

        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            CustomerRepository.Delete(id);
        }
    }
}
