﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SimpleCustomerDemo.Models;

namespace SimpleCustomerDemo.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FavoriteCustomersController : ControllerBase
    {
        [HttpGet]
        public ActionResult<IEnumerable<Customer>> Get()
        {
            var allCustomers = CustomerRepository.GetAll();
            var favoriteCustomers = new List<Customer>();
            foreach (var customer in allCustomers)
            {
                if (customer.IsFavorite)
                {
                    favoriteCustomers.Add(customer);
                }
            }
            return favoriteCustomers;
        }
        [HttpPost(Name= "MARKCUSTOMERASFAVORITE")]
        public IActionResult Post([FromBody]string id)
        {
            if (Uri.TryCreate(id,UriKind.RelativeOrAbsolute, out Uri uri))
            {
                var idSegment = uri.Segments.LastOrDefault();
                if (int.TryParse(idSegment, out int primaryKey))
                {
                    if (CustomerRepository.TryGet(primaryKey, out Customer customer))
                    {
                        customer.IsFavorite = true;
                        CustomerRepository.Update(customer);
                        return new OkResult();
                    }
                }
            }

            return new NotFoundResult();
        }
        [HttpPost(Name = "REMOVECUSTOMERASFAVORITE")]
        public IActionResult RemoveFavPost([FromBody]string id)
        {
            if (Uri.TryCreate(id, UriKind.RelativeOrAbsolute, out Uri uri))
            {
                var idSegment = uri.Segments.LastOrDefault();
                if (int.TryParse(idSegment, out int primaryKey))
                {
                    if (CustomerRepository.TryGet(primaryKey, out Customer customer))
                    {
                        customer.IsFavorite = false;
                        CustomerRepository.Update(customer);
                        return new OkResult();
                    }
                }
            }

            return new NotFoundResult();
        }
    }
}