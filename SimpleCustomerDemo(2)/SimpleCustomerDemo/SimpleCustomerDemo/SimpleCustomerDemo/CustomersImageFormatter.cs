﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Formatters;
using SimpleCustomerDemo.Models;

namespace SimpleCustomerDemo
{
    public class CustomersImageFormatter : OutputFormatter
    {
        private readonly IHostingEnvironment m_HostingEnvironment;

        public CustomersImageFormatter(IHostingEnvironment hostingEnvironment)
        {
            m_HostingEnvironment = hostingEnvironment;
            SupportedMediaTypes.Add("image/png");
            SupportedMediaTypes.Add("image/jpeg");
            SupportedMediaTypes.Add("image/jpg");
        }

        protected override bool CanWriteType(Type type)
        {
            if (typeof(Customer).IsAssignableFrom(type))
            {
                return base.CanWriteType(type);
            }

            return false;
        }

      public override async Task WriteResponseBodyAsync(OutputFormatterWriteContext context)
        {
            var responseStream = context.HttpContext.Response.Body;
            if (context.Object is Customer customer)
            {
                if (!string.IsNullOrWhiteSpace(customer.ImageFile))
                {
                    var path = Path.Combine(m_HostingEnvironment.WebRootPath, "images", customer.ImageFile);
                    var buffer = File.ReadAllBytes(path);
                    await responseStream.WriteAsync(buffer, 0, buffer.Length);
                }
            }
        }
    }
}