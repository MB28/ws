﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using SimpleCustomerDemo.Models;

namespace SimpleCustomerDemo.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CustomersController : ControllerBase
    {
        // GET api/values
        [HttpGet(Name = "GETALLCUSTOMERS")]
        public ActionResult<IEnumerable<Customer>> Get()
        {
            return CustomerRepository.GetAll();
        }

        // GET api/values/5
        [HttpGet("{id}", Name = "GETCUSTOMER")]
        public ActionResult<Customer> Get(int id)
        {
            if (CustomerRepository.TryGet(id, out Customer customer))
            {
                return new OkObjectResult(customer);
            }
            return new NotFoundResult();
        }

        // POST api/values
        [HttpPost]
        public IActionResult Post([FromBody] Customer value)
        {
            int newInternalId = CustomerRepository.Add(value);
            return new CreatedAtRouteResult("GETCUSTOMER", new { id = newInternalId }, null);
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] Customer value)
        {
            CustomerRepository.Update(value);

        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            CustomerRepository.Delete(id);
        }
    }
}
