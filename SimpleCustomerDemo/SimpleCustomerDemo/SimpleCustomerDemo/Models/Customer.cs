﻿using System;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace SimpleCustomerDemo.Models
{
    public class Customer
    {
        [IgnoreDataMember]
        public int Id { get; set; }
        public string Name { get; set; }
        public string Street { get; set; }
        public string ZipCode { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        [IgnoreDataMember]
        public string ImageFile { get; set; }
    }
}
