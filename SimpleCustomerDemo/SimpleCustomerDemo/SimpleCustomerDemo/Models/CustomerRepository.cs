﻿using System.Collections.Generic;

namespace SimpleCustomerDemo.Models
{
    public static class CustomerRepository
    {
        private static Dictionary<int, Customer> s_Customers = new Dictionary<int, Customer>();

        static CustomerRepository()
        {
            s_Customers.Add(1, new Customer
            {
                Id = 1,
                Name = "Hans Meier",
                Street = "Kaiserstr. 12",
                City = "Karlsruhe",
                ZipCode = null,
                Country = "DE"
            }
            );
            s_Customers.Add(2, new Customer
            {
                Id = 2,
                Name = "Otto Schmitt",
                Street = "Kriegsstr. 122",
                City = "Karlsruhe",
                ZipCode = "76133",
                Country = "DE"
            }
            );
            s_Customers.Add(3, new Customer
            {
                Id = 3,
                Name = "Chelsea Elizabeth Manning",
                Street = "Elrod Avenue",
                City = "Quantico",
                ZipCode = "VA22134",
                Country = "USA",
                ImageFile = "Manning.jpg"
            }
            );
        }

        public static List<Customer> GetAll()
        {
            lock (s_Customers)
            {
                return new List<Customer>(s_Customers.Values);
            }
        }

        public static Customer Get(int id)
        {
            return s_Customers[id];
        }

        public static bool TryGet(int id, out Customer customer)
        {
            return s_Customers.TryGetValue(id, out customer);
        }

        public static int Add(Customer customer)
        {
            var newId = s_Customers.Count + 1;
            s_Customers.Add(newId, customer);
            return newId;
        }

        public static void Update(Customer customer)
        {
            s_Customers[customer.Id] = customer;
        }

        public static void Delete(int id)
        {
            s_Customers.Remove(id);
        }
    }
}